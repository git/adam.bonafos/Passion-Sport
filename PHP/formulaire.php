<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retour formulaire</title>
    <link rel="icon" type="image/png" href="../media/images/icon.ico">
    <link rel="stylesheet" type="text/css" href="../CSS/style.css">
    <link rel="stylesheet" type="text/css" href="../CSS/page_php.css">

</head>
<body>
    <header>
        <nav class="menu">
            <ul class="menu_liste">
                <li><a href="../HTML/index.html">Home</a></li>
                <li><a href="../HTML/table.html">Galerie</a></li>
                <li><a href="../HTML/news.html">News</a></li>
                <li><a href="../HTML/actus.html">Actus</a></li>
                <li class="reseaux-mobile">
                    <a href="https://fr-fr.facebook.com/">
                        <img src="../media/images/logo/logo_facebook.webp">
                    </a>
                    <a href="https://twitter.com/?lang=fr">
                        <img src="../media/images/logo/logo_twitter.png">
                    </a>
                    <a href="https://www.instagram.com/?hl=fr">
                        <img src="../media/images/logo/logo_instagram.webp">
                    </a>
                    <a href="https://www.strava.com/?hl=fr">
                        <img src="../media/images/logo/logo_strava.png">
                    </a>
                </li>
                <li id="contact">Contact
                    <ul class="sous-menu">
                        <li><a href="../HTML/formulaire.html" class="active">Nous contacter</a></li>
                        <li><p>Nos adresses :</p>
                            <div>
                                <li class="coordonnees"><a href="tel:+33">Téléphone</a></li>
                                <li class="coordonnees"><a href="mailto:passionsport@contact.fr">Mail</a></li>
                                <li class="coordonnees"><a href="https://goo.gl/maps/1mVkDZjDFm7MkmB58">Adresse</a></li>
                            </div>
                        </li>
                        <li><a href="https://www.strava.com/?hl=fr">Strava</a></li>
                        <li>Instagram<img src="../media/images/qr_code/qr_instagram.png"></li>
                        <li>Twitter<img src="../media/images/qr_code/qr_twitter.webp"></li>
                        <li>Facebook<img src="../media/images/qr_code/qr_facebook.png"></li>
                        <li>Snapchat<img src="../media/images/qr_code/qr_snapchat.png"></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <div class="menu_button"><a href="#">ACCUEIL</a>
        <img src="../media/images/menu_button.png" class="menu_button_img">
    </div>

    <?php
    $nom = $_POST['nom'];
    $mail = $_POST['email'];
    $tel = $_POST['tel'];
    $motif = $_POST['motif'];
    $hor = $_POST['hor'];
    $message = $_POST['message'];
    $compt = 0;

?>
<div id="grp_infos">
    <div id="nom" class="infos">
    <?php
        if (empty($nom)) {
            echo "<b>Rentrez votre Nom</b>";
        }
        else {
            if (strlen($nom) > 1) {
                echo "Nom pris en compte : <em>$nom</em>";
            $compt = $compt + 1;
            }
            else {
                echo "<b>Nom trop court (minimum 2 caractères)</b>";
            }
        }
    ?>
    </div>

    
    <div id="mail" class="infos">
    <?php
        if (empty($mail)) {
            echo "<b>Rentrez votre adresse email</b>";
        }
        else {
            if (filter_var($mail)) { /*strlen($mail) >= 8 */
                echo "Adrese email prise en compte : <em>$mail</em>";
                $compt = $compt + 1;
            }
            else {
                echo "<b>Format d'adresse email invalide</b>";
            }
        }
    ?>
    </div>

    <div id="tel" class="infos">
        <?php
        if (! empty($tel)) {
            echo "Numéro de téléphone pris en compte : <em>$tel</em>";
        }
        ?>
    </div>

    <div id="motif" class="infos">
        <?php
        if ($motif != 'Aucun') {
            echo "Motif pris en compte : <em>$motif</em>";
        }
        ?>
    </div>

    <div id="horraire" class="infos">
        <?php
        if (! empty($hor)) {
            echo "Créneau horaire pris en compte : <em>$hor</em>";
        }
        ?>
    </div>

    <div id="message" class="infos">
    <?php
        if (empty($message)) {
            echo "<b>Rentrez votre message</b>";
        }
        else {
            if (strlen($message) >= 15) {
                echo "Message bien pris en compte : <em>$message</em>";
                $compt = $compt + 1;
            }
            else {
                echo "<b>Développé votre message (nombre de caractères minimum : 15)</b>";
            }    
        }
    ?>
    </div>

</div>

<div class="notif">
    <p id="valid">
    <?php
        if ($compt == 3) {
            echo "Votre message à été transmis à notre équipe";
        }
    ?>
    </p>
    <p id="rejet">
    <?php
        if ($compt != 3) {
            echo "Votre message a été rejeté, complété vos informations";
        }
    ?>
    </p>
</div>


</body>

<footer>Bonafos Adam G6</footer>
  
<script>
        const menu_mobile = document.querySelector(".menu_button_img")
        const menu = document.querySelector(".menu_liste")
 
        menu_mobile.addEventListener('click',()=>{
        menu.classList.toggle('mobile-menu')
        })
</script>
</html>